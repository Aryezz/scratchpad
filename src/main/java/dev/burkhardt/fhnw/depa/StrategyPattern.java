package dev.burkhardt.fhnw.depa;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class FilterInts {
    List<Integer> ints = IntStream.iterate(1, i -> i + 1)
            .boxed()
            .limit(100)
            .collect(Collectors.toList());
    Strategy strategy;
    public FilterInts(Strategy filter) {
        this.strategy = filter;
    }
    public List<Integer> getFiltered() {
        return strategy.filter(ints);
    }
}

interface Strategy { public List<Integer> filter(List<Integer> ints); }
class EvenOnly implements Strategy {
    public List<Integer> filter(List<Integer> ints) {
        return ints.stream().filter(i -> i % 2 == 0).toList();
    }
}
class SmallOnly implements Strategy {
    public List<Integer> filter(List<Integer> ints) {
        return ints.stream().filter(i -> i < 20).toList();
    }
}

class StrategyPatternTest {
    public static void main(String[] args) {
        System.out.println((new FilterInts(new EvenOnly())).getFiltered());
        System.out.println((new FilterInts(new SmallOnly())).getFiltered());
    }
}

