package dev.burkhardt.fhnw.depa;

import java.util.ArrayList;
import java.util.List;

interface Component { void operation(); }

class Composite {
    // use getters/setters in reality
    public List<Component> parts = new ArrayList<>();
    public void operation() {
        parts.forEach(p -> p.operation());
    }
}

public class CompositePattern {
    public static void main(String[] args) {
        Composite c = new Composite();
        c.parts.add(() -> System.out.println("Leaf 1"));
        c.parts.add(() -> System.out.println("Leaf 2"));
        c.operation();
    }
}
