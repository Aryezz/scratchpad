package dev.burkhardt.fhnw.depa;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Observer Aspekte:
 * 1. ConcurrentModificationException
 *    CopyOnWriteArrayList benutzen oder Liste vor und nach add/remove kopieren
 * 2. "Memory Leaks"
 *    Objekte die nicht mehr gebraucht werden aber noch als Observer registriert
 *    sind werden vom GC nicht gelöscht
 * 3. Zyklische Abhängigkeit
 *    Wird der StatePattern innerhalb eines Observers aktualisiert kann es zu rekursiven
 *    update calls kommen. Testen ob der state sich tatsächlich verändert hat
 *    kann helfen.
 * 4. Inkonsistente Updates
 *    Observer sollen erst notifiziert werden, wenn alle Änderungen am StatePattern
 *    durchgeführt wurden, sonst kann es zu einem inkonsistenten StatePattern kommen.
 * 5. Änderungen am Modell so lange verzögern, bis alle Observer über alle
 *    vorangegangenen Änderungen informiert worden sind.
 */
class Observable {
    private final List<Observer> observers = new CopyOnWriteArrayList<>();
    public int state = 0;

    public void updateState(int newState) {
        if (state == newState) return; // prevent recursive updates
        state = newState;
        notifyObservers();
    }

    public void addObserver(Observer o) { observers.add(o); }

    public void removeObserver(Observer o) { observers.remove(o); }

    protected void notifyObservers() {
        for (Observer observer : observers) {
            observer.update(this);
        }
    }
}

interface Observer {
    void update(Observable source);
}

class TestObserver {
    public static void main(String[] args) {
        Observable observable = new Observable();
        Observer observer = o -> System.out.println(o.state);
        observable.addObserver(observer);
        observable.updateState(1); // prints 1
        observable.removeObserver(observer);
        observable.updateState(100); // doesn't print
    }
}
