package dev.burkhardt.fhnw.depa;

class MutablePoint {
    private int x, y;
    // getters/setters

    public MutablePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void move(int dx, int dy) {
        x += dx;
        y += dy;
    }
}

class ImmutablePoint {
    private int x, y;
    // getters/setters

    public ImmutablePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public ImmutablePoint move(int dx, int dy) {
        return new ImmutablePoint(x + dx, y + dy);
    }
}
