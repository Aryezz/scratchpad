package dev.burkhardt.fhnw.depa;

interface State { public void handle(); }
class PingState implements State {
    Context context;
    public PingState(Context context) {
        this.context = context;
    }
    @Override
    public void handle() {
        System.out.println("PING!");
        context.setState(new PongState(context));
    }
}
class PongState implements State {
    Context context;

    public PongState(Context context) {
        this.context = context;
    }

    @Override
    public void handle() {
        System.out.println("PONG!");
        context.setState(new PingState(context));
    }
}
class Context {
    State state = new PingState(this);
    public void setState(State state) {
        this.state = state;
    }
    public void process() {
        state.handle();
    }
}
class StatePatternTest {
    public static void main(String[] args) {
        Context context = new Context();
        context.process(); // prints PING!
        context.process(); // prints PONG!
        context.process(); // prints PING!
    }
}



