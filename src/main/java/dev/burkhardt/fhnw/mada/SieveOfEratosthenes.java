package dev.burkhardt.fhnw.mada;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class SieveOfEratosthenes {
    private final int limit;
    private final boolean[] sieve;
    private boolean shaken = false;

    public SieveOfEratosthenes(int limit) {
        this.limit = limit;
        this.sieve = new boolean[limit];
        Arrays.fill(sieve, true);
    }

    private void shakeSieve() {
        for (int i = 2; i < limit; i++)
            if (sieve[i - 1])
                for (int j = 2 * i; j <= limit; j += i)
                    sieve[j - 1] = false;

        shaken = true;
    }

    public List<Integer> getPrimes() {
        if (!shaken)
            shakeSieve();

        List<Integer> primes = IntStream.range(1, limit).boxed().filter(i -> sieve[i]).toList();

        return primes;
    }
}
