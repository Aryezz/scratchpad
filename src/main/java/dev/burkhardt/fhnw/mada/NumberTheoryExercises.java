package dev.burkhardt.fhnw.mada;

import java.math.BigInteger;
import java.util.Random;

public class NumberTheoryExercises {
    public static void main(String[] args) {
        System.out.println("Exercise 4:");
        System.out.println(new SieveOfEratosthenes(100000).getPrimes());

        System.out.println("\nExercise 5:");
        int primeCount = new SieveOfEratosthenes(1000000).getPrimes().size();
        System.out.printf("Exact number of primes below 1'000'000: %d%n", primeCount);
        System.out.printf("Estimated lower bound: %.3f%n", 1000000 / (Math.log(1000000) + 2));
        System.out.printf("Estimated upper bound: %.3f%n", 1000000 / (Math.log(1000000) - 4));

        System.out.println("\nExercise 7:");
        Random random = new Random();
        BigInteger prime1 = BigInteger.probablePrime(1024, random);
        BigInteger prime2 = BigInteger.probablePrime(1024, random);
        System.out.println(prime1);
        System.out.println(prime2);
        System.out.println(prime1.multiply(prime2));
    }
}
