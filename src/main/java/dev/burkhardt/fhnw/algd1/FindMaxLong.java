package dev.burkhardt.fhnw.algd1;

/**
 * Finding the largest number that can be represented using a long without assuming any prior knowledge.
 */
public class FindMaxLong {
    public static void main(String[]args){
        long x = 1;
        int countops = 0;

        // double the counter until it wraps
        while (x * 2 > x) {
            x *= 2;
            countops++;
        }

        // we know that we cannot double the counter again, but counter/2 might still fit into the space
        long step = x / 2;
        while (step > 0) {
            // adding at most once suffices, because we know we can't add counter/2 twice
            if (x + step > x) {
                x += step;
                countops++;
            }

            step /= 2;
        }

        System.out.println("Biggest long: " + x);
        System.out.println("Control     : " + Long.MAX_VALUE);
        System.out.println("Operations  : " + countops);
    }
}