package dev.burkhardt.fhnw.algd1;

public class Backtracking {
    private static final int MAX_WEIGHT = 9;
    private static final int[] weight = {7, 6, 5, 3};
    private static final int[] value = {70, 54, 50, 24};
    private static int totWeight, totValue, maxValue;

    public static void main(String[] args) {
        System.out.println("max value: " + calcMaxValue());
    }

    public static int calcMaxValue() {
        maxValue = 0;
        pack(0);
        return maxValue;
    }

    private static void pack(int i) {
        if (totWeight > MAX_WEIGHT)
            return;

        if (i < weight.length) {
            pack(i + 1); // skip item
            packItem(i);
            pack(i + 1); // pack item
            unpackItem(i);
        } else if (totValue > maxValue) {
            maxValue = totValue;
            System.out.println("new max value: " + maxValue);
        }
    }

    private static void packItem(int i) {
        System.out.println("packing item " + i);
        totWeight += weight[i];
        totValue += value[i];
    }

    private static void unpackItem(int i) {
        System.out.println("unpacking item " + i);
        totWeight -= weight[i];
        totValue -= value[i];
    }
}
