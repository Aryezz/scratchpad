package dev.burkhardt.fhnw.algd1;

import java.util.Arrays;

public class MergeSort {
    public static void mergeSort(int[] a) {
        sort(a, 0, a.length);
    }

    public static void bottomUpMergeSort(int[] a) {
        for (int i = 1; i < a.length; i *= 2) {
            int j = 0;
            while (j + 2 * i < a.length) {
                merge(a, j, j + i, j + 2 * i);
                j = j + 2 * i;
            }
            if (j + i < a.length) {
                merge(a, j, j + i, a.length);
            }
        }
    }

    private static void sort(int[] a, int beg, int end) {
        if (end - beg > 1) {
            int m = (end + beg) >>> 1;
            sort(a, beg, m);
            sort(a, m, end);
            merge(a, beg, m, end);
        }
    }

    private static void merge(int[] a, int beg, int m, int end) {
        int[] b = new int[end - beg];

        int i = 0, j = beg, k = m;
        while (j < m && k < end) {
            if (a[j] <= a[k])  b[i++] = a[j++];
            else               b[i++] = a[k++];
        }

        // copy remaining values between a[j] and a[m] to end of b
        // not necessary for remaining values between a[k] and a[end]
        // because they are already correctly sorted in a
        while (j < m)
            b[i++] = a[j++];

        // overwrite a again
        while (i > 0) {
            i--;
            a[beg + i] = b[i];
        }
    }

    public static void main(String[] args) {
        int[] a = new int[] {3, 2, 1, 1, 1, 2, 3, 2, 1};
        bottomUpMergeSort(a);
        System.out.println(Arrays.toString(a));
    }
}
