package dev.burkhardt.fhnw.algd1;

public class Binsearch {
    static int binSearch(int[] data, int value) {
        int l = -1, h = data.length;
        while (l + 1 < h) {
            int m = (l + h) >>> 1;
            if (data[m] < value)
                l = m;
            else if (data[m] > value)
                h = m;
            else // data[m] == value
                return m;
        }
        return -1; // not found
    }

    static int binSearchFirst(int[] data, int value) {
        int l = -1, h = data.length;
        while (l + 1 < h) {
            int m = (l + h) >>> 1;
            if (data[m] >= value)
                h = m;
            else
                l = m;
        }
        return (h < data.length && data[h] == value) ? h : -1;
    }

    public static void main(String[] args) {
        int[] arr = new int[] {1, 2, 2, 3, 3, 3};
        System.out.println(binSearch(arr, 3)); // 4
        System.out.println(binSearchFirst(arr, 3)); // 3
    }
}
