package dev.burkhardt.fhnw.algd1;

import java.util.Arrays;

public class AustrianNationalFlag {
    enum FlagColour {  RED, WHITE  }

    private static void sortAustrian(FlagColour[] a) {
        int n = a.length;
        int r = 0, w = 0, r2 = n;

        while (w < r2) {
            if (a[w] == FlagColour.WHITE)  w++;
            else if (r == n - r2) {
                swap(a, w, r);
                r++;
                w++;
            } else {
                swap(a, w, r2 - 1);
                r2--;
            }
        }
    }

    private static void swap(FlagColour[] a, int i, int j) {
        FlagColour t = a[i];
        a[i] = a[j];
        a[j] = t;
    }

    public static void main(String[] args) {
        FlagColour r = FlagColour.RED, w = FlagColour.WHITE;
        FlagColour[] flag = new FlagColour[] {r, r, w, r, r, w, r, w, w, w};

        sortAustrian(flag);
        System.out.println(Arrays.toString(flag));
    }
}
