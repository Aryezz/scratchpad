package dev.burkhardt.fhnw.algd1;

import java.util.Arrays;

public class DutchNationalFlag {
    enum FlagColour {  RED, WHITE, BLUE  }

    private static void sortDutch(FlagColour[] a) {
        int r = 0, w = 0, b = a.length;

        while(w < b) {
            if (a[w] == FlagColour.RED) {
                a[w] = a[r];  // swap (no temp needed)
                a[r] = FlagColour.RED;
                r++; w++;
            } else if (a[w] == FlagColour.BLUE) {
                a[w] = a[b - 1];  // swap
                a[b - 1] = FlagColour.BLUE;
                b--;
            } else {
                w++;
            }
        }
    }

    public static void main(String[] args) {
        FlagColour r = FlagColour.RED, w = FlagColour.WHITE, b = FlagColour.BLUE;
        FlagColour[] flag = new FlagColour[] {r, b, w, r, w, b };

        sortDutch(flag);
        System.out.println(Arrays.toString(flag));
    }
}
