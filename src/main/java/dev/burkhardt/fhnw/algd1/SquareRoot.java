package dev.burkhardt.fhnw.algd1;

public class SquareRoot {
    public static double squareRoot(double x) {
        if (x < 0) return Double.NaN;
        double l = 0, h = Double.MAX_VALUE;
        double lastm = Double.NaN;

        double m = l + (h - l) / 2;
        while (m != lastm) {
            lastm = m;
            if (m * m < x)
                l = m;
            else
                h = m;
            m = l + (h - l) / 2;
        }

        return x - l * l <= h * h - x ? l : h;
    }

    public static void main(String[] args) {
        // 1.414213562373095
        System.out.println(squareRoot(2));
    }
}
