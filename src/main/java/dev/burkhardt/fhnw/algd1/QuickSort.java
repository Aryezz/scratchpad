package dev.burkhardt.fhnw.algd1;

import java.util.Arrays;

public class QuickSort {
    public static void quickSort(int[] a) {
        sort(a, 0, a.length - 1);
    }

    public static void sort(int[] a, int beg, int end) {
        while (end > beg) {
            System.out.print("beg=" + beg + ", end=" + end);
            int p = a[(beg + end) >>> 1]; // pivot element
            System.out.println(", p=" + p);
            int i = beg, j = end;

            while (i <= j) {
                while (a[i] < p) i++;
                while (a[j] > p) j--;

                if (i <= j) {
                    int t = a[i];
                    a[i] = a[j];
                    a[j] = t;

                    i++;
                    j--;
                }
            }

            // only sort smaller partition recursively, other partition
            // gets sorted using loop this reduces worst-case recursion
            // depth to O(log n) from from O(n), since only the smaller
            // partition (which is at most n/2 elements long) adds a
            // recursive call
            if (j - beg <= end - i) {
                sort(a, beg, j);
                beg = i;
            } else {
                sort(a, i, end);
                end = j;
            }
        }
    }

    public static void main(String[] args) {
        int[] a = new int[] {39, 17, 17, 25, 48, 7, 9};
        quickSort(a);
        System.out.println(Arrays.toString(a));
    }
}
