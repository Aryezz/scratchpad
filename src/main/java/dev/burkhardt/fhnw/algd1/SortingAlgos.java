package dev.burkhardt.fhnw.algd1;

import java.util.Arrays;

public class SortingAlgos {
    static void selectionSort(int[] a) {
        int n = a.length;
        for (int i = 0; i < n - 1; i++) {
            int min = i; // min index
            for (int j = i + 1; j < n; j++) {
                if (a[j] < a[min])  min = j;
            }
            // swap
            int t = a[min];
            a[min] = a[i];
            a[i] = t;
        }
    }

    static void insertionSort(int[] a) {
        int n = a.length;
        for (int i = 1; i < n; i++) {
            // sequential search
            int j = i, t = a[j];
            while (j > 0 && a[j - 1] > t) {
                a[j] = a[j - 1]; // shift
                j--;
            }
            a[j] = t;
        }
    }

    static void insertionSortBin(int[] a) {
        int n = a.length;
        for (int i = 1; i < n; i++) {
            // binary search
            int l = -1, h = i, t = a[i];
            while (l + 1 < h) {
                int m = (l + h) >>> 1;
                if (a[m] <= t)  l = m;
                else  h = m;
            }
            for (int j = i; j > h; j--)
                a[j] = a[j - 1];
            a[h] = t;
        }
    }

    static void bubbleSort(int[] a) {
        int n = a.length;
        for (int i = 0; i < n; i++) {
            for (int j = n - 1; j > i; j--) {
                if (a[j] < a[j - 1]) {
                    // swap
                    int t = a[j - 1];
                    a[j - 1] = a[j];
                    a[j] = t;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] a = new int[] {3, 2, 1, 1, 1, 2, 3, 2, 1};
        bubbleSort(a);
        insertionSort(a);
        insertionSortBin(a);
        selectionSort(a);
        System.out.println(Arrays.toString(a));
    }
}
